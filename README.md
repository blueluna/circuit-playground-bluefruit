# Rust experiments with Adafruit Circuit Playground Bluefruit

This is a work in progress crate for the Adafruit Circuit Playground Bluefruit
board (CPB).

## Generating a UF2 image

Get the uf2conv.py script from the uf2 repository, https://github.com/Microsoft/uf2.

 1. Build the binary in release mode
    ```
    $ cargo build --examples --release
    ```
 2. Generate a hex file using,
    ```
	$ objdump -O ihex ./target/thumbv7em-none-eabihf/release/examples/uart uart.hex
	```
 3. Genrate the UF2 image,
    ```
	$ python3 uf2conv.py ./uart.hex -c -f 0xada52840 -o uart.uf2
	```

## CDC

```
adafruit-nrfutil dfu genpkg --dev-type 0x0052 --application uart.hex dfu-package.zip
```

```
adafruit-nrfutil dfu serial --package dfu-package.zip -p /dev/ttyACM1 -b 115200
```

## Restoring the boot loader

Download the Nordic command line tools,
https://www.nordicsemi.com/Software-and-Tools/Development-Tools/nRF-Command-Line-Tools/Download#infotabs

Download the boot loader,
https://github.com/adafruit/Adafruit_nRF52_Bootloader/releases

Program the boot loader using nrfjprog,
```
nrfjprog --program bootloader_binary.hex --chiperase -f nrf52 --reset
```

## License

Licensed under the MIT license.
